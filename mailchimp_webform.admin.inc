<?php

/**
 * @file
 * Admin functionality for Mailchimp Webforms.
 */

/**
 * Form constructor for the mailchimp management form.
 */
function mailchimp_webform_components_form($form, &$form_state, $webform_node) {
  $q = mailchimp_get_api_object();
  if (!$q) {
    drupal_set_message(t('Could not get valid Mailchimp API object'), 'error');
  }

  $nid = $webform_node->nid;
  $record = mailchimp_webform_load($nid);
  $form['#record'] = $record;
  $records_count = 1;

  $list_options = mailchimp_webform_get_list_options();
  if ($record) {
    $records_count = count($record->data);
  }

  $form['#tree'] = TRUE;
  $form['#node'] = $webform_node;
  $form['mailchimp'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mailchimp Settings'),
    '#prefix' => '<div id="mailchimp-fieldset-wrapper">',
    '#suffix' => '</div>',
  );

  // Figure out how many items to display.
  if (empty($form_state['num_items'])) {
    $count = 1;
    $form_state['num_items'] = $records_count;
  }

  $form_state['field_deltas'] = isset($form_state['field_deltas']) ? $form_state['field_deltas'] : range(0, $records_count - 1);

  foreach ($form_state['field_deltas'] as $key => $item) {
    $data = [];
    if (isset($record->data[$key])) {
      $data = $record->data[$key];
    }

    mailchimp_webform_set_settings_fieldset($form, $form_state, $webform_node, $key, $list_options, $data);
  }

  $form['add_list'] = [
    '#type' => 'submit',
    '#value' => t('Add another list'),
    '#submit' => ['mailchimp_webform_add_more'],
    '#ajax' => [
      'callback' => 'mailchimp_webform_add_more_callback',
      'wrapper' => 'mailchimp-fieldset-wrapper',
    ],
  ];

  $form['actions']['save'] = [
    '#type' => 'submit',
    '#value' => t('Save'),
  ];

  $form['actions']['delete'] = [
    '#type' => 'submit',
    '#value' => t('Reset'),
    '#submit' => ['mailchimp_webform_components_form_delete_submit'],
    '#attributes' => [
      'onclick' => 'if(!confirm("All mappings will be removed for this webform. Are you sure you want to continue?")){return false;}',
    ],
  ];

  return $form;
}

/**
 * Set mailchimp setting fieldset.
 *
 * @param array &$form
 *   Form element.
 * @param array &$form_state
 *   Form state element.
 * @param object $webform_node
 *   Webform object.
 * @param int $key
 *   The fieldset key.
 * @param mixed $list_options
 *   Contains options to be used in a select list.
 * @param array $record
 *   Array of settings.
 */
function mailchimp_webform_set_settings_fieldset(array &$form, array &$form_state, $webform_node, $key, $list_options, array $record) {
  $form['mailchimp'][$key] = [
    '#type' => 'fieldset',
    '#title' => t('List @count Settings', ['@count' => $key + 1]),
  ];

  $form['mailchimp'][$key]['is_active'] = [
    '#type' => 'checkbox',
    '#title' => t('Is active'),
    '#default_value' => isset($record['is_active']) ? $record['is_active'] : FALSE,
  ];

  $form['mailchimp'][$key]['mailchimp_list'] = [
    '#type' => 'select',
    '#title' => t('Choose list'),
    '#options' => $list_options ? $list_options : [],
    '#ajax' => [
      'callback' => 'mailchimp_webform_add_more_callback',
      'wrapper' => 'mailchimp-fieldset-wrapper',
    ],
    '#default_value' => isset($record['mailchimp_list']) ? $record['mailchimp_list'] : FALSE,
  ];

  $form['mailchimp'][$key]['merge_fields'] = [
    '#type' => 'fieldset',
    '#title' => t('Merge Fields'),
    '#description' => t('Map Webform Fields to Mailchimp merge fields.'),
    '#id' => 'mergefields-wrapper',
  ];

  $form['mailchimp'][$key]['merge_fields']['table_start'] = [
    '#markup' => '<table>
                    <tr>
                      <th>Name</th>
                      <th>Type</th>
                      <th>Mailchimp Merge Field</th>
                    </tr>',
  ];

  $list = isset($form_state['values']['mailchimp'][$key]['mailchimp_list']) ? $form_state['values']['mailchimp'][$key]['mailchimp_list'] : '';

  // If loading an already saved record, display the selected list.
  if (!$list) {
    if (isset($record['mailchimp_list'])) {
      $list = $record['mailchimp_list'];
    }
  }

  $merge_field_options = mailchimp_webform_get_mergefields_options($list);

  foreach ($webform_node->webform['components'] as $k => $component) {
    $row_class = ($k % 2) ? 'odd' : 'even';
    $form['mailchimp'][$key]['merge_fields']['components'][$k] = [
      '#prefix' => '<tr class="' . $row_class . '">
                     <td>' . $component['name'] . '</td>
                     <td>' . $component['type'] . '</td>
                     <td>',
      '#component' => $component,
      'key' => [
        '#type' => 'select',
        '#options' => $merge_field_options,
        '#default_value' => isset($record['merge_fields']['components'][$k]['key']) ? $record['merge_fields']['components'][$k]['key'] : '',
      ],
      '#suffix' => '</td></tr>',
    ];
  }

  $form['mailchimp'][$key]['merge_fields']['table_end'] = [
    '#markup' => '</table>',
  ];

  $form['mailchimp'][$key]['remove'] = [
    '#type' => 'submit',
    '#value' => t('Remove'),
    '#submit' => ['mailchimp_webform_remove_one'],
    '#name' => 'remove_item_' . $key,
    '#ajax' => [
      'callback' => 'mailchimp_webform_add_more_callback',
      'wrapper' => 'mailchimp-fieldset-wrapper',
    ],
  ];
}

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function mailchimp_webform_add_more_callback($form, &$form_state) {
  return $form['mailchimp'];
}

/**
 * Submit handler for the "remove" button.
 *
 * Decrements the number of items counter and causes a rebuild.
 */
function mailchimp_webform_remove_one($form, &$form_state) {
  $delta_remove = $form_state['triggering_element']['#parents'][1];
  $key = array_search($delta_remove, $form_state['field_deltas']);
  unset($form_state['field_deltas'][$key]);
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the number of items counter and causes a rebuild.
 */
function mailchimp_webform_add_more($form, &$form_state) {
  $form_state['field_deltas'][] = count($form_state['field_deltas']) > 0 ? max($form_state['field_deltas']) + 1 : 0;
  $form_state['rebuild'] = TRUE;
}

/**
 * Form submission handler for mailchimp_webform_components_form().
 */
function mailchimp_webform_components_form_submit($form, $form_state) {
  $node = $form['#node'];
  $record = $form['#record'];
  if ($record) {
    $update = ['nid'];
  }
  else {
    $record = new stdClass();
    $update = [];
  }

  if (!empty($form_state['values']['mailchimp'])) {
    $record->data = $form_state['values']['mailchimp'];
  }

  $record->nid = $node->nid;

  drupal_write_record('mailchimp_webform', $record, $update);
}

/**
 * Form submission handler for mailchimp_webform_components_form().
 */
function mailchimp_webform_components_form_delete_submit($form, &$form_state) {
  $node = $form['#node'];
  mailchimp_webform_delete($node->nid);
  drupal_set_message(t('Mailchimp settings for this webform deleted.'), 'status');
}
