<?php

/**
 * @file
 * Hooks provided by the Mailchimp Webform module.
 */

/**
 * All other modules to modify the merge_vars data.
 *
 * @param array &$merge_vars
 *   Mailchimp merge vars array.
 * @param array $form
 *   Form object.
 * @param array $form_state
 *   Form state object.
 */
function hook_mailchimp_webform_recipient_alter(array &$merge_vars, array &$form, array &$form_state) {
  if (isset($merge_vars['consent'])) {
    $date = new DateTime();
    $merge_vars['consent'] = $date->format('Y-m-d H:i:s');
  }
}
